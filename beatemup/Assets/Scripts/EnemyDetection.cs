using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour
{
    public event Action<Transform> OnPlayerEnter;
    public event Action<Transform> OnPlayerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnPlayerEnter?.Invoke(collision.gameObject.transform);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        OnPlayerExit?.Invoke(collision.gameObject.transform);
    }
}
