using BarraDeVida;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public void GoPelea()
    {
        GameManager.Instance.ChangeScene(GameManager.PeleaScene);
    }

    public void GoGameOver()
    {
        GameManager.Instance.ChangeScene(GameManager.GameOverScene);
    }
}
