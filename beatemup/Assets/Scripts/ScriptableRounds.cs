using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableRounds", menuName = "ScriptableObjects/ScriptableRounds")]
public class ScriptableRounds : ScriptableObject
{
    public int numWave;
    public int numEnemies;
    public int numLeft;
}
