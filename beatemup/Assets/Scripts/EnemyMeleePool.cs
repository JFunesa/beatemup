using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleePool : MonoBehaviour
{
    public static EnemyMeleePool instance;
    private List<GameObject> pooledEnemy = new List<GameObject>();
    private int amountPool = 10;

    [SerializeField] 
    private GameObject enemy;

    private void Awake()
    {
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }

    void Start()
    {
        for (int i = 0; i < amountPool; i++)
        {
            GameObject obj = Instantiate(enemy);
            obj.SetActive(false);
            pooledEnemy.Add(obj);
        }
    }

    public GameObject GetPooledEnemyMelee()
    {
        for (int i = 0; i < pooledEnemy.Count; i++)
        {
            if (!pooledEnemy[i].activeInHierarchy)
            {
                return pooledEnemy[i];
            }
        }
        return null;
    }
}
