using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableVida", menuName = "ScriptableObjects/ScriptableVida")]
public class ScriptableVida : ScriptableObject
{
    public float vida;
    public float vidaMax;
    public Vector3 pos;
}
