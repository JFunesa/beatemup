using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<ScriptableEnemics> meleeList;
    public List<ScriptableEnemics> rangedList;
    public ScriptableRounds waveinfo;
    public GameEvent ChangeWave;

    // Start is called before the first frame update
    void Start()
    {
        waveinfo.numWave = 0;
        waveinfo.numLeft = 0;
        waveinfo.numEnemies = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (waveinfo.numLeft == 0)
        {
            setWave();
            ChangeWave.Raise();
        }
    }

    public void setWave()
    {
        ++waveinfo.numWave;

        if (waveinfo.numWave == 1)
        {
            spawnEnemyMelee(meleeList[0]);

            ++waveinfo.numEnemies;
            waveinfo.numLeft = waveinfo.numEnemies;



        }
        else if (waveinfo.numWave == 2)
        {
            spawnEnemyRanged(rangedList[0]);
            waveinfo.numLeft = waveinfo.numEnemies;
        }
        else if (waveinfo.numWave > 2 && waveinfo.numWave <= 5)
        {
            ++waveinfo.numEnemies;
            waveinfo.numLeft = waveinfo.numEnemies;
            for (int i = 0; i < waveinfo.numEnemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[0]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[0]);
                }
            }


        }
        else if (waveinfo.numWave > 5 && waveinfo.numWave <= 9)
        {
            ++waveinfo.numEnemies;
            waveinfo.numLeft = waveinfo.numEnemies;
            for (int i = 0; i < waveinfo.numEnemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[1]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[1]);
                }
            }


        }
        else
        {
            ++waveinfo.numEnemies;
            waveinfo.numLeft = waveinfo.numEnemies;
            for (int i = 0; i < waveinfo.numEnemies; ++i)
            {
                int r0 = Random.Range(1, 3);
                if (r0 == 1)
                {
                    spawnEnemyMelee(meleeList[2]);
                }
                else
                {
                    spawnEnemyRanged(rangedList[2]);
                }
            }


        }
    }

    public void spawnEnemyMelee(ScriptableEnemics enemyLvL)
    {
        GameObject newenemy = EnemyMeleePool.instance.GetPooledEnemyMelee();
        if (newenemy != null)
        {
            newenemy.SetActive(true);
            newenemy.GetComponent<EnemySwitchMachine>().inicialitzar(enemyLvL);
            int r = Random.Range(1, 3);
            bool izq = false;
            if (r == 1) izq = true;
            if (izq)
            {
                newenemy.GetComponent<Transform>().position = new Vector2(Random.Range(-9, -1.5f), -2);
            }
            else
            {
                newenemy.GetComponent<Transform>().position = new Vector2(Random.Range(1.5f, 9), -2);
            }


        }
    }
    public void spawnEnemyRanged(ScriptableEnemics enemyRangedLvL)
    {
        GameObject newenemy = EnemyRangedPool.instance.GetPooledEnemyRanged();
        if (newenemy != null)
        {
            newenemy.SetActive(true);
            newenemy.GetComponent<EnemySwitchMachine>().inicialitzar(enemyRangedLvL);
            int r = Random.Range(1, 3);
            bool izq = false;
            if (r == 1) izq = true;
            if (izq)
            {
                float rpos = Random.Range(-9, -1.5f);
                newenemy.GetComponent<Transform>().position = new Vector2(rpos, -2);
            }
            else
            {
                float rpos = Random.Range(1.5f, 9);
                newenemy.GetComponent<Transform>().position = new Vector2(rpos, -2);
            }

        }
    }
}