
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEditor.Timeline.Actions;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;


public class EnemySwitchMachine : MonoBehaviour
{

    //Aquesta m�quina funciona amb un enum pels estats i un switch
    //per a la gesti� de la l�gica de cada un.
    //Imprescindible utilitzar les seves funcions de canvi, actualitzaci�,
    //inici i sortida de cada estat.
    //None simplement existeix perqu� hem de fer l'init del primer estat i cal que
    //la variable no tingui valor igual al primer d'ells (idle).
    private enum SwitchMachineStates { NONE, IDLE, WALK, PATROLL, HIT1, DAMAGE};
    private SwitchMachineStates m_CurrentState;
    private SwitchMachineStates m_DamageReturnState;

    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per� evitem que un estat entri a si mateix
        //�s possible que la nostra m�quina ho permeti, per tant aix�
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        Debug.Log(this.name +  " :" + m_CurrentState + " -> " + newState);

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("idle");
                this.GetComponent<SpriteRenderer>().color = Color.white;
                break;

            case SwitchMachineStates.WALK:

                m_Animator.Play("walk");
                this.GetComponent<SpriteRenderer>().color = Color.gray;
                break;

            case SwitchMachineStates.PATROLL:

                    
                this.GetComponent<SpriteRenderer>().color = Color.black;
                break;

            case SwitchMachineStates.HIT1:
                m_Rigidbody.velocity = Vector2.zero;
                m_HitboxInfo.Damage = m_Hit1Damage;
                if (m_DetectionAttack.gameObject.GetComponent<CircleCollider2D>().radius <= 0.3f)
                {
                    m_Animator.Play("hit1");

                }
                else
                {
                    StartCoroutine(SpawnBalas());
                }



                break;

            case SwitchMachineStates.DAMAGE:
                    
                m_Animator.Play("damage");
                m_DamageReturnState = m_CurrentState;
                m_Rigidbody.velocity = Vector2.zero;
                this.GetComponent<SpriteRenderer>().color = Color.red;
                if (m_vida <= 0)
                {
                    rounds.numLeft -= 1;
                    this.gameObject.SetActive(false);
                }
                else
                {
                    StartCoroutine(DamageCoroutine());
                }
                

                break;

            default:
                break;
        }
        m_CurrentState = currentState;
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopAllCoroutines();

                break;

            case SwitchMachineStates.WALK:
                StopAllCoroutines();

                break;

            case SwitchMachineStates.PATROLL:
                StopAllCoroutines();

                break;

            case SwitchMachineStates.HIT1:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.DAMAGE:
                StopAllCoroutines();
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (m_Rigidbody.velocity != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                else if (m_patrulla)
                    ChangeState(SwitchMachineStates.PATROLL);

                break;
            case SwitchMachineStates.WALK:
                m_Rigidbody.velocity = (m_Transform.position - transform.position).normalized * m_Speed;
                if (m_Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                    transform.eulerAngles = new Vector3(0, 180, 0);
                if (m_Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                    transform.eulerAngles = Vector3.zero;

                if (m_Rigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                else if (m_patrulla)
                    ChangeState(SwitchMachineStates.PATROLL);

                break;

            case SwitchMachineStates.PATROLL:
                if (m_patrulla)
                {
                    if (this.transform.position.x >= punto1.x)
                    {
                        modopatrulla = false;
                        Debug.Log(modopatrulla);
                    }
                    if (this.transform.position.x <= punto2.x)
                    {
                        modopatrulla = true;
                        Debug.Log(modopatrulla + " 2");
                    }

                    if (modopatrulla)
                        m_Rigidbody.velocity = (punto1 - this.transform.position).normalized * m_Speed;
                    else
                        m_Rigidbody.velocity = (punto2 - this.transform.position).normalized * m_Speed;


                    if (m_Rigidbody.velocity.x < 0 && transform.eulerAngles.y != 180)
                        transform.eulerAngles = new Vector3(0, 180, 0);
                    if (m_Rigidbody.velocity.x > 0 && transform.eulerAngles.y != 0)
                        transform.eulerAngles = Vector3.zero;
                }
                else
                {
                    ChangeState(SwitchMachineStates.WALK);
                }
                    


                break;

            case SwitchMachineStates.HIT1:
                    
                    

                break;

            case SwitchMachineStates.DAMAGE:
                


                break;

            default:
                break;
        }
    }

    IEnumerator DamageCoroutine()
    {
        m_vida -= playerHit;
        yield return new WaitForSeconds(0.5f);
        ChangeState(m_DamageReturnState);
        
    }

    IEnumerator SpawnBalas()
    {
        while (true)
        {
            GameObject dany = Instantiate(m_Bala);
            dany.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y);
            dany.GetComponent<Rigidbody2D>().velocity = (m_Transform.position - transform.position).normalized * (m_Speed + 4);
            yield return new WaitForSeconds(2f);
        }
            
    }


    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    } 
    //------------------------------------------------------------------------//

    //------------------------------------------------------------------------//
    //Input
    //Podriem anar-nos subscrivint i desubscrivint de les diferents accions segons
    //l'estat en el que ens trobem.

    private void AttackAction()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.PATROLL:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.DAMAGE:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            default:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)     
    {
        Debug.Log(string.Format("Enemy trigger enter: {0}", collision.name));
        if (collision.gameObject.layer == LayerMask.NameToLayer("HitBoxPlayer"))
        {
            playerHit = collision.gameObject.GetComponent<HitboxInfo>().Damage;
            ChangeState(SwitchMachineStates.DAMAGE);
            collision.gameObject.TryGetComponent<HitboxInfo>(out HitboxInfo hitbox);
            Debug.Log(string.Format("Hola I've been hit by {0} and it did {1} damage to me",
                collision.name,
                hitbox?.Damage));
        }
    }

    private void OnPlayerChase(Transform player)
    {
        m_Transform = player;
        Debug.Log("Perseguint al player " + player.gameObject);
        if (!m_patrulla)
            ChangeState(SwitchMachineStates.WALK);
        else
            ChangeState(SwitchMachineStates.PATROLL);


    }

    private void OnStopChase(Transform player)
    {
        punto1 = this.transform.position + offset1;
        punto2 = this.transform.position + offset2;
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnPlayerAttack(Transform player)
    {
        m_Transform = player;
        AttackAction();
    }


    //------------------------------------------------------------------------//

        
    private Rigidbody2D m_Rigidbody;
    private Transform m_Transform;
    [SerializeField]
    private HitboxInfo m_HitboxInfo;
    [SerializeField]
    private GameObject m_Bala;
    [SerializeField] 
    EnemyDetection m_Detection;
    [SerializeField]
    EnemyDetection m_DetectionAttack;
    private int playerHit;
    private bool modopatrulla = true;


    [Header("Character Values")]
    [SerializeField]
    private ScriptableVida player;
    [SerializeField]
    private ScriptableRounds rounds;
    [SerializeField]
    private float m_Speed;
    private int m_Hit1Damage;
    [SerializeField]
    private int m_vida;
    private Vector3 offset1;
    private Vector3 offset2;
    private Vector3 punto1;
    private Vector3 punto2;
    private Animator m_Animator;
    private bool m_patrulla;


    public void inicialitzar(ScriptableEnemics enemic)
    {
        m_Speed = enemic.m_Speed;
        m_vida = enemic.vida;
        m_Hit1Damage = enemic.m_Hit1Damage;
        m_Bala.GetComponent<HitboxInfo>().Damage = enemic.m_Hit1Damage;
        m_Detection.gameObject.GetComponent<CircleCollider2D>().radius = enemic.radiusDetection;
        m_DetectionAttack.gameObject.GetComponent<CircleCollider2D>().radius = enemic.radiusDetectionAttack;
        m_DetectionAttack.gameObject.GetComponent<CircleCollider2D>().offset = new Vector2(enemic.offsetDetectionAttack, 0);
        m_Animator.runtimeAnimatorController = enemic.m_Animator;
        m_Transform = this.transform;
        offset1 = enemic.offset1;
        offset2 = enemic.offset2;
        punto1 = transform.position + offset1;
        punto2 = transform.position + offset2;
        m_patrulla = enemic.patrulla;
        if (m_patrulla)
            m_Detection.gameObject.SetActive(false);
        

    }

    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();



        m_Detection.OnPlayerEnter += OnPlayerChase;
        m_Detection.OnPlayerExit += OnStopChase;
        m_DetectionAttack.OnPlayerEnter += OnPlayerAttack;
        m_DetectionAttack.OnPlayerExit += OnPlayerChase;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.PATROLL);
    }

    void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        m_Detection.OnPlayerEnter -= OnPlayerChase;
        m_Detection.OnPlayerExit -= OnStopChase;
        m_DetectionAttack.OnPlayerEnter -= OnPlayerAttack;
        m_DetectionAttack.OnPlayerExit -= OnPlayerChase;
    }
}

