using UnityEngine;
using UnityEngine.SceneManagement;

namespace BarraDeVida
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager m_Instance;
        public static GameManager Instance => m_Instance;

        public const string GameOverScene = "GameOver";
        public const string PeleaScene = "Pelea";
        public const string MenuScene = "Menu";


        private void Awake()
        {

            if (m_Instance == null)
            {
                m_Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;

            ChangeScene(MenuScene);
        }

        //Es crida al carregar una escena
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("GameManager - OnSceneLoaded: " + scene.name);
        }

        public void ChangeScene(string scene)
        {
            SceneManager.LoadScene(scene);
        }

    }
}
