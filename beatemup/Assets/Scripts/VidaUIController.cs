using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour
{
    private Image barraComida;
    [SerializeField]
    private ScriptableVida Player;
    void Start()
    {
        barraComida = GetComponent<Image>();
        mostrar();
    }

    public void mostrar()
    {
        barraComida.fillAmount = Player.vida / Player.vidaMax;
    }
}
