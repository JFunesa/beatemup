using m17;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableEnemics", menuName = "ScriptableObjects/ScriptableEnemics")]
public class ScriptableEnemics : ScriptableObject
{
    public float m_Speed;
    public int m_Hit1Damage;
    public int vida;
    public Vector3 offset1;
    public Vector3 offset2;
    public AnimatorController m_Animator;
    public float radiusDetection;
    public float radiusDetectionAttack;
    public float offsetDetectionAttack;
    public bool patrulla = true;
}
