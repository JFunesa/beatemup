using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionAttack : MonoBehaviour
{
    public Action<Collider2D> OnPlayerEnter;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnPlayerEnter?.Invoke(collision);
    }
}
