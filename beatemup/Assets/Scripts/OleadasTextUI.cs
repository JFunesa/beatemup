using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OleadasTextUI : MonoBehaviour
{
    public ScriptableRounds rounds;
    // Start is called before the first frame update
    void Start()
    {
        mostrar();
    }

    public void mostrar()
    {
        this.gameObject.GetComponent<TextMeshProUGUI>().text = "Onada: " + rounds.numWave + "";
    }
}
