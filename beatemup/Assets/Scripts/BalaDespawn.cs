using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaDespawn : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Muro") {
            Destroy(this.gameObject);
        }

    }
}
