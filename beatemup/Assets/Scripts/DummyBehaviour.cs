using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class DummyBehaviour : MonoBehaviour
    {
        [SerializeField] EnemyDetection m_Detection;

        private void Awake()
        {
            m_Detection.OnPlayerEnter += OnPlayerChase;
        }

        private void OnDestroy()
        {
            m_Detection.OnPlayerEnter -= OnPlayerChase;
        }

        private void OnPlayerChase(Transform player)
        {
            Debug.Log("Perseguint al player " + player.gameObject);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log(string.Format("Enemy trigger enter: {0}", collision.name));

            if (collision.gameObject.layer == LayerMask.NameToLayer("HitBoxPlayer"))
            {
                collision.gameObject.TryGetComponent<HitboxInfo>(out HitboxInfo hitbox);
                Debug.Log(string.Format("Hola I've been hit by {0} and it did {1} damage to me",
                    collision.name,
                    hitbox?.Damage));
            }
        }
    }
}

